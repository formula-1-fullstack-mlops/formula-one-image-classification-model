teams = ['Mercedes', 'Ferrari', 'Red Bull Racing', 'Haas', 'BWT', 'Alpha Tauri', 'McLaren', 'Alfa Romeo Racing', 'Williams', 'Renault']

teams_classes = {
  "Mercedes": "mercedes",
  "Ferrari": "ferrari",
  "Red Bull Racing": "redbull",
  "Haas": "haas",
  "BWT": "bwt",
  "Alpha Tauri": "toro_rosso",
  "McLaren": "mclaren",
  "Alfa Romeo Racing": "alfa_romeo",
  "Williams": "williams",
  "Renault": "renault",
}

colors_teams = {
  "Mercedes": "#00D2BE",
  "Ferrari": "#C00000",
  "Red Bull Racing": "#0600EF",
  "Haas": "#787878",
  "BWT": "#F596C8",
  "Alpha Tauri": "#C8C8C8",
  "McLaren": "#FF8700",
  "Alfa Romeo Racing": "#960000",
  "Williams": "#0082FA",
  "Renault": "#FFF500",
}